import React, {Component} from 'react';
import {connect} from "dva"
import {Button,notification, Col, Tabs, DatePicker, Form, Icon, Input, Modal, Row, Table} from 'antd'

@connect(({task}) => ({task}))
class Task extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'task/getOpenTasks'
    })
    dispatch({
      type: 'task/getClosedTasks'
    })
    dispatch({
      type: 'task/getCompletedTasks'
    })
  }

  render() {
    const {task, dispatch, form} = this.props;
    const {openTaskList, closedTaskList, completedTaskList, modalType, modalVisible, startDate, endDate} = task;
    const {getFieldDecorator, getFieldsValue, resetFields} = this.props.form;
    const {MonthPicker, RangePicker} = DatePicker;
    const {TabPane} = Tabs;
    const dateFormatList = ['DD/MM/YYYY', 'DD/MM/YY'];
    const onAddTask = () => {
      dispatch({
        type: 'task/updateState',
        payload: {
          modalVisible: true
        }
      })
      resetFields()
    };

    const closeModal = () => {
      dispatch({
        type: 'task/updateState',
        payload: {
          modalVisible: false
        }
      })
    };

    const deleteTask = (index) => {
      dispatch({
        type: 'task/deleteTask',
        payload: index
      })
    }
    const completeTask = (index) => {
      dispatch({
        type: 'task/completeTask',
        payload: index
      })
    }

    const closeTask = (index) => {
      dispatch({
        type: 'task/closeTask',
        payload: index
      })
    }


    const visibleColumnsOpen = [
      {
        title: 'Title',
        dataIndex: 'title',
        key: 'title'
      },
      {
        title: 'Interval Date',
        dataIndex: 'range',
        key: 'range',
        render: (text, record) => (record.startTime + "").substring(0, 10) + "  ~  " + (record.endTime + "").substring(0, 10),
      },
      {
        title: 'Operation',
        key: 'operation',
        render: (text, record) => <div>
          <Button onClick={() => completeTask(record.id)}><Icon type="check"/></Button>
          <Button onClick={() => closeTask(record.id)}><Icon type="close"/></Button>
          <Button type="danger" onClick={() => deleteTask(record.id)}><Icon type="delete"/></Button>
        </div>
      },
    ];
    const visibleColumnsClosed = [
      {
        title: 'Title',
        dataIndex: 'title',
        key: 'title'
      },
      {
        title: 'Interval Date',
        dataIndex: 'range',
        key: 'range',
        render: (text, record) => (record.startTime + "").substring(0, 10) + "  ~  " + (record.endTime + "").substring(0, 10),
      },
      {
        title: 'Operation',
        key: 'operation',
        render: (text, record) => <div>
          <Button type="danger" onClick={() => deleteTask(record.id)}><Icon type="delete"/></Button>
        </div>
      },

    ];
    const visibleColumnsCompleted = [
      {
        title: 'Title',
        dataIndex: 'title',
        key: 'title'
      },
      {
        title: 'Interval Date',
        dataIndex: 'range',
        key: 'range',
        render: (text, record) => (record.startTime + "").substring(0, 10) + "  ~  " + (record.endTime + "").substring(0, 10),
      },
      {
        title: 'Operation',
        key: 'operation',
        render: (text, record) => <div>
          <Button type="danger" onClick={() => deleteTask(record.id)}><Icon type="delete"/></Button>
        </div>
      },

    ];
    const rangeChange = (date, dateString) => {
      console.log(dateString);
      // start = new Date(start).getTime();
      let startDate = dateString[0];
      let endDate = dateString[1];

      // startDate =new Date(start).getTime(),
      // endDate =new Date(end).getTime(),
      dispatch({
        type: "task/updateState",
        payload: {
          startDate, endDate
        }
      })
    }
    const handleOk = () => {
      // console.log(startDate);
      // if (modalType === "create") {
      this.props.form.validateFields((err, value) => {
        let title = value.title;
        if (!err) {
          dispatch({
            type: 'task/addTask',
            payload: {
              title,
              startDate,
              endDate
            }
          });
          dispatch({
            type: 'task/updateState',
            payload: {
              modalVisible: false
            }
          });
          resetFields()
        }
      });
    }

    const sign = () => {
      dispatch({
        type: 'task/signIn'
      });
      dispatch({
        type: 'task/getOpenTasks'
      });
      dispatch({
        type: 'task/getClosedTasks'
      });
      dispatch({
        type: 'task/getCompletedTasks'
      })
    };


    return (
      <div>
        <Row>
          <Col span={6} offset={9}>
            <h1>Open Tasks :</h1>
          </Col>
          <Col span={4} offset={10}>
            <Button type={"primary"} onClick={onAddTask}>Add Task</Button>
            <Button type={"danger"} onClick={sign}> Sign In</Button>
          </Col>
        </Row>

        <hr/>
        <Row>
          <Col span={18} offset={3}>{console.log(openTaskList)}
            <Table dataSource={openTaskList} columns={visibleColumnsOpen} pagination={false}/>
          </Col>
        </Row>
        {/*<Button onClick={handleGetTasks}> Get Tasks</Button>*/}
        <hr/>
        <Row>
          <Col span={6} offset={9}>
            <h1>Task History</h1>
          </Col>
        </Row>
        <Row>
          <Col span={18} offset={3}>
            <Tabs defaultActiveKey="1">
              <TabPane tab="Completed Tasks" key="1">
                <Row>
                  <Col>{console.log(completedTaskList)}
                    <Table dataSource={completedTaskList} columns={visibleColumnsCompleted} pagination={false}/>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tab="Closed Tasks" key="2">
                <Row>
                  <Col>{console.log(closedTaskList)}
                    <Table dataSource={closedTaskList} columns={visibleColumnsClosed} pagination={false}/>
                  </Col>
                </Row>
              </TabPane>

            </Tabs>


          </Col>

        </Row>
        <Modal
          title="Basic Modal"
          visible={modalVisible}
          onOk={handleOk}
          onCancel={closeModal}
        >
          <Form>
            <Form.Item>
              {getFieldDecorator('title', {
                rules: [{required: true, message: 'Please input task Title!'}],
              })(
                <Input placeholder="Title"/>,
              )}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator('range', {
                rules: [{required: true, message: 'Please input date!'}],
              })(
                <RangePicker onChange={rangeChange} format={dateFormatList}/>
              )}
            </Form.Item>
          </Form>
        </Modal>

      </div>
    );
  }
}

export default Form.create()(Task)
