import {addTask, deleteTask, editTask, closeTask, completeTask, getOpenTasks,getClosedTasks,getCompletedTasks,signIn} from "./service";
import {notification} from 'antd'
export default {
  namespace: 'task',
  state: {
    currentId:'',
    openTaskList: [],
    closedTaskList: [],
    completedTaskList: [],
    modalVisible: false,
    currentItem: '',
    modalType: 'create',
    startDate: null,
    endDate: null
  },
  subscriptions: {},

  effects: {

    * signIn({payload}, {call, put, select}) {
      const data = yield call(signIn, {username: '+998912631453', password: 'root123'});
      localStorage.setItem('orderToken', data.data.accessToken);
      if (data.data.success) {
        yield put({
          type: 'getOpenTasks',
        });
        yield put({
          type: 'getClosedTasks',
        });
        yield put({
          type: 'getCompletedTasks',
        });
      }
    },
    * editTask({payload}, {call, put, select}) {
      const data = yield call(editTask, payload);
      yield put({
        type: 'task/getOpenTasks'
      });
      yield put({
        type: 'task/getClosedTasks'
      });
      yield put({
        type: 'task/getCompletedTasks'
      })
    },
    * addTask({payload}, {call, put, select}) {
      // payload.expenseDate = new Date(payload.expenseDate).getTime();
      console.log(payload)
      const data = yield call(addTask, payload);
      console.log(data)

      if(data.data.success){
        notification['success']({
          message: 'Qo\'shildi',
          description:'Vazifa muvaffaqiyatli qo\'shildi! ',
          placement: 'top',
          delay:2
        });
        yield put({
          type: 'task/getOpenTasks'
        });
        yield put({
          type: 'task/getClosedTasks'
        });
        yield put({
          type: 'task/getCompletedTasks'
        })

      }else{
        notification['error']({
          message:'Xatolik',
          description:'Bu vaqt oralig\'ida vazifa qo\'shib bo\'lmaydi!',
          placement: 'top',
          delay:2
        });
        yield put({
          type:'updateState',
          payload:{
            modalVisible: true
          }
        })
      }



    },

    * deleteTask({payload}, {call, put, select}) {
      const data = yield call(deleteTask, payload)
      if (data.data.success) {
        yield put({
          type: 'task/getOpenTasks'
        });
        yield put({
          type: 'task/getClosedTasks'
        });
        yield put({
          type: 'task/getCompletedTasks'
        })
      }
    },

    * completeTask({payload}, {call, put, select}) {
      const data = yield call(completeTask, payload)
      if (data.data.success) {
        yield put({
          type: 'task/getOpenTasks'
        });
        yield put({
          type: 'task/getClosedTasks'
        });
        yield put({
          type: 'task/getCompletedTasks'
        })
      }
    },

    * closeTask({payload}, {call, put, select}) {
      const data = yield call(closeTask, payload)
      if (data.data.success) {
        yield put({
          type: 'task/getOpenTasks'
        });
        yield put({
          type: 'task/getClosedTasks'
        });
        yield put({
          type: 'task/getCompletedTasks'
        })
      }
    },

    * getOpenTasks({payload}, {call, put, select}) {
      const data = yield call(getOpenTasks);
      console.log(data.data);
      yield put({
        type: 'updateState',
        payload: {
          openTaskList: data.data,

        }
      })
    },
    * getClosedTasks({payload}, {call, put, select}) {
      const data = yield call(getClosedTasks);
      console.log(data.data);
      yield put({
        type: 'updateState',
        payload: {
          closedTaskList: data.data,

        }
      })
    },
    * getCompletedTasks({payload}, {call, put, select}) {
      const data = yield call(getCompletedTasks);
      console.log(data.data);
      yield put({
        type: 'updateState',
        payload: {
          completedTaskList: data.data,

        }
      })
    },

  },


  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload,
      }
    },
  }
}
