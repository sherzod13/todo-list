import axios from 'axios'

export function signIn(data) {
  return axios.post('/api/auth/login',data,{headers:{'Authorization':'Bearer '+localStorage.getItem('orderToken')}})
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .finally(function () {
      // always executed
    });
}

export function getOpenTasks() {
  return axios.get('/api/task/open',{headers:{'Authorization':'Bearer '+localStorage.getItem('orderToken')}})
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .finally(function () {
      // always executed
    });
}

export function getClosedTasks() {
  return axios.get('/api/task/closed',{headers:{'Authorization':'Bearer '+localStorage.getItem('orderToken')}})
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .finally(function () {
      // always executed
    });
}

export function getCompletedTasks() {
  return axios.get('/api/task/completed',{headers:{'Authorization':'Bearer '+localStorage.getItem('orderToken')}})
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .finally(function () {
      // always executed
    });
}

export function editTask(data) {
  return axios.put('/api/task/'+data.id,data,{headers:{'Authorization':'Bearer '+localStorage.getItem('orderToken')}})
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .finally(function () {
      // always executed
    });
}

export function addTask(data) {
  return axios.post('/api/task', data,{headers:{'Authorization':'Bearer '+localStorage.getItem('orderToken')}})
    .then(function (response) {
      return response;
      console.log(response)
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .finally(function () {
      // always executed
    });
}

export function deleteTask(data) {
  console.log(data)
  return axios.delete('/api/task/'+data,{headers:{'Authorization':'Bearer '+localStorage.getItem('orderToken')}})
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .finally(function () {
      // always executed
    });
}

export function completeTask(data) {
  console.log(data)
  return axios.put('/api/task/completeTask/'+data,{headers:{'Authorization':'Bearer '+localStorage.getItem('orderToken')}})
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .finally(function () {
      // always executed
    });
}

export function closeTask(data) {
  console.log(data)
  return axios.put('/api/task/closeTask/'+data,{headers:{'Authorization':'Bearer '+localStorage.getItem('orderToken')}})
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .finally(function () {
      // always executed
    });
}



