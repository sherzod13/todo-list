package uz.pdp.apporderservice.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.apporderservice.entity.Task;
import uz.pdp.apporderservice.entity.enums.TaskStatus;
import uz.pdp.apporderservice.payload.ApiResponse;
import uz.pdp.apporderservice.payload.ReqTask;
import uz.pdp.apporderservice.payload.ReqTaskStatus;
import uz.pdp.apporderservice.repository.TaskRepository;


import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/task")
public class TaskController {

    @Autowired
    TaskRepository taskRepository;

    @GetMapping("/open")
    public HttpEntity<?> getOpenTasks() {
        return ResponseEntity.ok(taskRepository.findAllByStatus(TaskStatus.OPEN));
    }

    @GetMapping("/completed")
    public HttpEntity<?> getCompletedTasks() {
        return ResponseEntity.ok(taskRepository.findAllByStatus(TaskStatus.COMPLETED));
    }

    @GetMapping("/closed")
    public HttpEntity<?> getClosedTasks() {
        return ResponseEntity.ok(taskRepository.findAllByStatus(TaskStatus.CLOSED));
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteTask(@PathVariable UUID id) {
        if (taskRepository.findById(id).isPresent()) {
            taskRepository.deleteById(id);
            return ResponseEntity.ok(new ApiResponse("success", true));
        }

        return ResponseEntity.ok(new ApiResponse("error", false));

    }

    @PostMapping
    public HttpEntity<?> addTask(@RequestBody ReqTask reqTask) {

        try {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date parsedStartDate = dateFormat.parse(reqTask.getStartDate());
            Date parsedEndDate = dateFormat.parse(reqTask.getEndDate());
            Timestamp timestampStart = new java.sql.Timestamp(parsedStartDate.getTime());
            Timestamp timestampEnd = new java.sql.Timestamp(parsedEndDate.getTime());
            Task task = new Task(
                    reqTask.getTitle(),
                    timestampStart,
                    timestampEnd,
                    TaskStatus.OPEN
            );
            int a=0;
            List<Task> all = taskRepository.findAllByStatus(TaskStatus.OPEN);
            for (Task task1 : all) {
                if(!(timestampStart.before(task1.getStartTime()) && timestampEnd.before(task1.getStartTime()) || timestampStart.after(task1.getEndTime()) && timestampEnd.after(task1.getEndTime()))){
                    a++;
                }
            }

            if(a==0){
                taskRepository.save(task);
                return ResponseEntity.ok(new ApiResponse("success", true));
            }
            return ResponseEntity.ok(new ApiResponse("error", false));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.ok(new ApiResponse("error", false));
        }
    }

    @PutMapping("/{id}")
    public HttpEntity<?> editTask(@PathVariable UUID id, @RequestBody ReqTask reqTask) {
        if (taskRepository.findById(id).isPresent()) {
//            Task task = taskRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Task not found"));
//            task.setTitle(reqTask.getTitle());
//            task.setStartTime(reqTask.getStartDate());
//            task.setEndTime(reqTask.getEndDate());
//            task.setStatus(reqTask.getStatus());
//            taskRepository.save(task);
            return ResponseEntity.ok(new ApiResponse("success", true));
        }

        return ResponseEntity.ok(new ApiResponse("error", false));

    }

    @PutMapping("/completeTask/{id}")
    public HttpEntity<?> completeTask(@PathVariable UUID id) {
        if (taskRepository.findById(id).isPresent()) {
            Task task = taskRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Task not found"));
//            task.setStatus(reqTaskStatus.getStatus());
            task.setStatus(TaskStatus.COMPLETED);
            taskRepository.save(task);
            return ResponseEntity.ok(new ApiResponse("success", true));
        }

        return ResponseEntity.ok(new ApiResponse("error", false));

    }

    @PutMapping("/closeTask/{id}")
    public HttpEntity<?> closeTask(@PathVariable UUID id) {
        if (taskRepository.findById(id).isPresent()) {
            Task task = taskRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Task not found"));
//            task.setStatus(reqTaskStatus.getStatus());
            task.setStatus(TaskStatus.CLOSED);
            taskRepository.save(task);
            return ResponseEntity.ok(new ApiResponse("success", true));
        }

        return ResponseEntity.ok(new ApiResponse("error", false));

    }
}
