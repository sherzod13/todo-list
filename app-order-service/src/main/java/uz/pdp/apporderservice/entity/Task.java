package uz.pdp.apporderservice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.apporderservice.entity.enums.TaskStatus;
import uz.pdp.apporderservice.entity.template.AbsEntity;

import javax.persistence.Entity;
import java.sql.Timestamp;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Task extends AbsEntity {
    private String title;
    private Timestamp startTime;
    private Timestamp endTime;
    private TaskStatus status;
}
