package uz.pdp.apporderservice.payload;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.apporderservice.entity.enums.TaskStatus;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqTaskStatus {
    private TaskStatus status;
}
