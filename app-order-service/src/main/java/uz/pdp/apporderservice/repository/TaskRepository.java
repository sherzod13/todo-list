package uz.pdp.apporderservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.apporderservice.entity.Task;
import uz.pdp.apporderservice.entity.enums.TaskStatus;

import java.util.List;
import java.util.UUID;

public interface TaskRepository extends JpaRepository<Task, UUID> {
    List<Task> findAllByStatus(TaskStatus taskStatus);

}
